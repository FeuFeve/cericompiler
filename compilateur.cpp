//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2009 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


/* GOAL :
 * 
 * Program := [DeclarationPart] StatementPart
 * DeclarationPart := "[" Letter {"," Letter} "]"
 * StatementPart := Statement {";" Statement} "."
 * Statement := AssignementStatement
 * AssignementStatement := Letter "=" Expression
 * 
 * Expression := SimpleExpression [RelationalOperator SimpleExpression]
 * SimpleExpression := Term {AdditiveOperator Term}
 * Term := Factor {MultiplicativeOperator Factor}
 * Factor := Number | Letter | "(" Expression ")"| "!" Factor
 * Number := Digit{Digit}
 * 
 * AdditiveOperator := "+" | "-" | "||"
 * MultiplicativeOperator := "*" | "/" | "%" | "&&"
 * RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
 * Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
 * Letter := "a"|...|"z"
 * 
 */


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

enum OPREL {equ, diff, sup, inf, supe, infe, unknown};
char current, lookedAhead;                // Current char    
int NLookedAhead=0;
unsigned long TagNumber=0;

void ReadChar(void){
    if(NLookedAhead>0){
        current=lookedAhead;    // Char has already been read
        NLookedAhead--;
    }
    else
        // Read character and skip spaces until 
        // non space character is read
        while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
    NLookedAhead++;
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}
	
void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}

// Number := Digit {Digit}
void Number(void)
{
	unsigned long long number;
	if (current<'0' or current>'9')
		Error("Chiffre attendu");
	// a digit has been read
	number = current - '0';	// convert current (string) to int
	ReadChar();
	while (current>='0' and current<='9')
	{
		number *= 10;	// shift one digit to the left
		number += current - '0';
		ReadChar();
	}
	cout << "\tpush $" << number << endl;
}

void OpRel(void){
	if(current!='=' and current!='<' and current!='>')
		Error("Opérateur relationnel attendu");	// Comparator expected
	else
		ReadChar();
}

void Expression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		Expression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else if (current>='0' && current <='9')
		Number();
	else
		Error("'(', chiffre ou opérateur de comparaison attendu");
}

void SimpleExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}

}

OPREL RelationalOperator(void)
{
	if (current!='=' and current!='!' and current!='<' and current!='>')
		return unknown;
	LookAhead();
	if (lookedAhead == '=')
	{
		switch(current)
		{
			case '=':
				ReadChar();
				ReadChar();
				return equ;
			case '!':
				ReadChar();
				ReadChar();
				return diff;
			case '>':
				ReadChar();
				ReadChar();
				return supe;
			case '<':
				ReadChar();
				ReadChar();
				return infe;
		}
	}
	switch(current)
	{
		case '>':
			ReadChar();
			return sup;
		case '<':
			ReadChar();
			return inf;
		case '=':
			Error("utilisez '==' comme opérateur d'égalité");
		default:
			Error("opérateur relationnel inconnu");
	}
	return unknown;	// unreachable code
}

void Expression(void){
	OPREL oprel;
	SimpleExpression();
	if(current=='=' or current=='!' or current=='<' or current=='>'){
		oprel = RelationalOperator();
		SimpleExpression();
		
		cout << "\tpop %rbx" << endl;			// get first operand
		cout << "\tpop %rax" << endl;			// get second operand
		cout << "\tcmpq %rax, %rbx" << endl;	// compare them
		switch(oprel){
			case equ:
				cout << "\tje Vrai" << ++TagNumber << "\t# If equal" << endl;
				break;
			case diff:
				cout << "\tjne Vrai" << ++TagNumber << "\t# If different" << endl;
				break;
			case inf:
				cout << "\tjb Vrai" << ++TagNumber << "\t# If below" << endl;
				break;
			case infe:
				cout << "\tjbe Vrai" << ++TagNumber << "\t# If below or equal" << endl;
				break;
			case sup:
				cout << "\tja Vrai" << ++TagNumber << "\t# If above" << endl;
				break;
			case supe:
				cout << "\tjae Vrai" << ++TagNumber << "\t# If above or equal" << endl;
				break;
			default:
				Error("Opérateur relationnel attendu");
		}
		
		cout << "\tpush $0\t\t# False" << endl;
		cout << "\tjmp Suite" << TagNumber << endl;
		cout << "Vrai" << TagNumber << ":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True" << endl;	
		cout << "Suite" << TagNumber << ":" << endl;
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	Expression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





